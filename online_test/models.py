from django.db import models
from django.contrib.auth.models import User
from djorm_pgarray.fields import ArrayField
from datetime import datetime, timedelta

# Create your models here.


class Candidate(User):

    experience_choice = (('0-1', '0-1'), ('2-3', '2-3'), ('3+', '3+'), ('5+', '5+'),
                         ('8+', '8+'), ('12+', '12+'))

    experience = models.CharField(max_length=4, choices=experience_choice, default='0-1')
    address = models.TextField()
    phone_no = models.CharField(max_length=12)
    primary_skill = models.CharField(max_length=30,
                                     help_text="Please provide Primary language like Python,Php,Java")
    skill_set = models.TextField()
    assign_test = ArrayField()
    attend_on = models.DateTimeField()
    full_name = models.CharField(max_length=150, null=True)

    def get_full_name(self):

        return self.first_name+' '+self.last_name

    def __unicode__(self):

        return self.get_full_name()

    @property
    def check_previous_attempt(self):

        check_email = Candidate.objects.filter(email=self.email)

        if check_email.values():

            prev_test_list = check_email.filter(email_id=self.email,\
                                                  attend_on_gte=self.attend_on-timedelta(days=6*365/12),\
                                                  attend_on_lt=self.attend_on).values('attend_on').order_by('-attend_on')
        else:
            return []
        # Need to find user is writing test within 6 months logic

        return prev_test_list

    def save(self, *args, **kwargs):

        prev_attempt = self.check_previous_attempt
        self.full_name = self.get_full_name()
        self.attend_on = datetime.now()
        if prev_attempt:

            return None

        super(Candidate, self).save(*args, **kwargs)


class Test(models.Model):

    type_choice = (('Quiz', 'quiz'), ('Pgm', 'Program'))
    lang_choice = (('java', 'Java'), ('py', 'Python'), ('.net', '.Net'), ('Php', 'PHP'))
    test_name = models.CharField(max_length=40)
    test_type = models.CharField(max_length=40, choices=type_choice)
    language = models.CharField(max_length=35, choices=lang_choice)
    duration = models.IntegerField(default=30)
    created_on = models.DateTimeField()

    def __unicode__(self):

        return self.test_name


class Questions(models.Model):

    question = models.TextField()
    test_id = models.ForeignKey(Test)
    diffculty = models.CharField(max_length=10)
    created_on = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):

        return self.question


class Answer(models.Model):

    answer = models.TextField(null=True, blank=True)
    candidate_id = models.ForeignKey(Candidate)
    question_id = models.ForeignKey(Questions)
    answered_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):

        return self.candidate_id.username+' '+str(self.answered_at.date())+' '+self.answer







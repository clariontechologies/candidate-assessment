from django.forms import ModelForm
from .models import Candidate


class CandidateRegisterForm(ModelForm):

    class Meta:
        model = Candidate
        fields = ('username', 'password', 'email',
                        'first_name', 'last_name',
                        'experience', 'primary_skill', 'skill_set')






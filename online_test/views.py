from django.shortcuts import render
from django.views.generic import ListView, View, FormView
from django.views.generic.edit import CreateView, UpdateView
from .models import Candidate, Answer, Questions, Test
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_protect
from .forms import CandidateRegisterForm
from django.template import RequestContext
from django.shortcuts import render_to_response
import simplejson
import subprocess
# Create your views here.


class Register(CreateView):

    template_name = "test/registeration.html"
    form_class = CandidateRegisterForm
    fields = ['password']
    success_url = '/tests/list'

    def form_valid(self, form):

        return super(Register, self).form_valid(form)


class TestListing(ListView):

    model = Test

    pass


class QuizTest(ListView):
    model = Questions
    template_name = "test/quiz.html"

    def get_queryset(self):

        self.questions = Questions.objects.get(id=1)

    def get_context_data(self, **kwargs):

        context = super(QuizTest, self).get_context_data(**kwargs)

        context['questions'] = self.questions

        return context


class PgmTest(ListView):
    model = Questions
    #paginate_by = 1
    template_name = "test/new_pgm_test.html"

    def get_queryset(self):

        self.questions = Questions.objects.all().values()

    def get_context_data(self, **kwargs):

        context = super(PgmTest, self).get_context_data(**kwargs)

        context['questions'] = self.questions

        return context


@csrf_protect
def execute_code(request):

    get_data = request.GET.get('code', "")
    qid = request.GET.get('qid', 1)
    base_file = '/temp/'+str(request.user)+'_'+str(qid)
    file_name_py = base_file + '.py'
    output_file_txt = base_file + '_out.txt'

    file_obj = open(str(file_name_py), 'w')

    file_obj.write(get_data)
    file_obj.flush()
    file_obj.close()
    with open(output_file_txt, "w") as output:
        subprocess.call(['python', file_name_py], stdout=output, stderr=output)

    result = file(output_file_txt).read()
    param = dict()

    param['content'] = get_data

    param['result'] = result

    return HttpResponse(simplejson.dumps(param), mimetype="application/json")











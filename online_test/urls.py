from django.conf.urls import patterns, include, url
from .views import *

urlpatterns = patterns('',
    url(r'^quiztest/', QuizTest.as_view(), name="quiz_test"),
    url(r'^pgmtest/', PgmTest.as_view(), name="quiz_test"),
    url(r'^execute_code/', execute_code, name="execute_code"),
    url(r'^register/', Register.as_view(), name="register"),
)

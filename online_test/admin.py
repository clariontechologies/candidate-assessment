from django.contrib import admin
from .models import Candidate, Questions, Answer, Test

class CadidateAdmin(admin.ModelAdmin):

    fields = ('username', 'password', 'email', 'first_name', 'last_name',
              'experience', 'primary_skill', )



admin.site.register(Candidate, CadidateAdmin)
admin.site.register(Questions)
admin.site.register(Answer)
admin.site.register(Test)
